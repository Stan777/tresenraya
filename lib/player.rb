class Player

  def initialize
    @score = 0
  end

  def getScore
    @score
  end

  def addScore
    @score = @score + 1
  end

  def restartScore
    @score = 0
  end
end