require './lib/board'
require './lib/player'

class Game

  def initialize
    @board = Board.new()
    @turn = 1
    @player1 = Player.new()
    @player2 = Player.new()
  end

  def getTurn
    @turn
  end

  def applyMove(pos)
    @board.applyMove(pos, @turn)
    @turn = @turn * -1
  end

  def getBoard
    @board.getBoard
  end

  def getScorePlayer1
    @player1.getScore
  end

  def addScorePlayer1
    @player1.addScore
  end

  def getScorePlayer2
    @player2.getScore
  end

  def addScorePlayer2
    @player2.addScore
  end

  def checkWinner
    @board.checkWinner
  end

  def isFinalize
    @board.isFinalize
  end

  def restartGame
    @board.restartTable
    @turn = 1
  end
end
