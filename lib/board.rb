class Board

  def initialize
    @table = [0, 0, 0, 0, 0, 0, 0, 0, 0]
  end

  def getBoard
    @table
  end

  def applyMove(pos, move)
    @table[pos] = move
  end

  def isFinalize
    response = true
    for i in 0..8
      if @table[i] == 0
        response = false
      end
    end
    response
  end

  def restartTable
    @table = [0, 0, 0, 0, 0, 0, 0, 0, 0]
  end
  
  def checkWinner
    for i in [-1, 1]
      if(@table[0] == i && @table[1] == i && @table[2] == i)
        return i
      end
      if(@table[3] == i && @table[4] == i && @table[5] == i)
        return i
      end
      if(@table[6] == i && @table[7] == i && @table[8] == i)
        return i
      end
      if(@table[0] == i && @table[3] == i && @table[6] == i)
        return i
      end
      if(@table[1] == i && @table[4] == i && @table[7] == i)
        return i
      end
      if(@table[2] == i && @table[5] == i && @table[8] == i)
        return i
      end
      if(@table[0] == i && @table[4] == i && @table[8] == i)
        return i
      end
      if(@table[2] == i && @table[4] == i && @table[6] == i)
        return i
      end
    end
    0
  end

end
