require './lib/game'

describe Game do

  before(:each) do
    @game = Game.new()
  end

  it "deberia retornar el turno del jugador1" do
    expect(@game.getTurn).to eq(1)
  end

  it "deberia retornar el turno del jugador2" do
    @game.applyMove(0)
    expect(@game.getTurn).to eq(-1)
  end

  it "deberia retornar el turno del jugador1 despues del turno del jugador2" do
    @game.applyMove(0)
    @game.applyMove(1)
    expect(@game.getTurn).to eq(1)
  end

  it "deberia retornar un tablero vacio" do
    expect(@game.getBoard).to eq([0, 0, 0, 0, 0, 0, 0, 0, 0])
  end

  it "deberia retornar un tablero con un movimiento del jugador1" do
    @game.applyMove(0)
    expect(@game.getBoard).to eq([1, 0, 0, 0, 0, 0, 0, 0, 0])
  end

  it "deberia retornar el puntaje inicial del jugador1" do
    expect(@game.getScorePlayer1).to eq(0)
  end

  it "deberia agregarse un punto al jugador1" do
    @game.addScorePlayer1
    expect(@game.getScorePlayer1).to eq(1)
  end

  it "deberia retornar el puntaje inicial del jugador2" do
    expect(@game.getScorePlayer2).to eq(0)
  end

  it "deberia agregarse un punto al jugador2" do
    @game.addScorePlayer2
    expect(@game.getScorePlayer2).to eq(1)
  end

  it "deberia retornar como ganador al jugador1" do
    @game.applyMove(0)
    @game.applyMove(3)
    @game.applyMove(1)
    @game.applyMove(4)
    @game.applyMove(2)
    expect(@game.checkWinner).to eq(1)
  end

  it "deberia retornar como ganador al jugador2" do
    @game.applyMove(0)
    @game.applyMove(3)
    @game.applyMove(1)
    @game.applyMove(4)
    @game.applyMove(7)
    @game.applyMove(5)
    expect(@game.checkWinner).to eq(-1)
  end

  it "deberia retornar como ganador a ningun jugador" do
    @game.applyMove(1)
    @game.applyMove(0)
    @game.applyMove(2)
    @game.applyMove(5)
    @game.applyMove(3)
    @game.applyMove(6)
    @game.applyMove(4)
    @game.applyMove(7)
    expect(@game.checkWinner).to eq(0)
    expect(@game.isFinalize).to eq(false)
  end

  it "deberia retornar empate" do
    @game.applyMove(1)
    @game.applyMove(0)
    @game.applyMove(2)
    @game.applyMove(5)
    @game.applyMove(3)
    @game.applyMove(6)
    @game.applyMove(4)
    @game.applyMove(7)
    @game.applyMove(8)
    expect(@game.checkWinner).to eq(0)
    expect(@game.isFinalize).to eq(true)
  end

  it "deberia reiniciar el tablero" do
    @game.applyMove(1)
    @game.applyMove(0)
    @game.applyMove(2)
    @game.applyMove(5)
    @game.applyMove(3)
    @game.applyMove(6)
    @game.applyMove(4)
    @game.applyMove(7)
    @game.applyMove(8)
    @game.restartGame
    expect(@game.getBoard).to eq([0, 0, 0, 0, 0, 0, 0, 0, 0])
  end

  it "deberia retornar el turno del jugador1 cuando reinicio el juego" do
    @game.applyMove(1)
    @game.restartGame
    expect(@game.getTurn).to eq(1)
  end
end
