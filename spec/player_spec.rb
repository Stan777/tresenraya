require './lib/player'

describe Player do

  before(:each) do
    @player = Player.new()
  end
  
  it "el jugador deberia empezar con puntaje cero" do
    expect(@player.getScore).to eq(0)
  end

  it "el jugador deberia aumentar su puntaje a uno" do
    @player.addScore
    expect(@player.getScore).to eq(1)
  end

  it "el jugador deberia aumentar su puntaje dos veces" do
    @player.addScore
    @player.addScore
    expect(@player.getScore).to eq(2)
  end

  it "deberia reiniciar el score del jugador" do
    @player.addScore
    @player.restartScore
    expect(@player.getScore).to eq(0)
  end
end
