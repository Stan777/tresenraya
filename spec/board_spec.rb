require './lib/board'

describe Board do

  before(:each) do
    @board = Board.new()
  end
  
  it "el tablero tiene que estar vacio" do
    expect(@board.getBoard).to eq([0, 0, 0, 0, 0, 0, 0, 0, 0])
  end
  
  it "deberia llenar una equis en la posicion cero del tablero" do
    @board.applyMove(0, 1)
    expect(@board.getBoard).to eq([1, 0, 0, 0, 0, 0, 0, 0, 0])
  end

  it "deberia llenar un circulo en la posicion cero del tablero" do
    @board.applyMove(0, -1)
    expect(@board.getBoard).to eq([-1, 0, 0, 0, 0, 0, 0, 0, 0])
  end

  it "deberia llenar una equis en la posicion cinco del tablero" do
    @board.applyMove(5, 1)
    expect(@board.getBoard).to eq([0, 0, 0, 0, 0, 1, 0, 0, 0])
  end

  it "finalize deberia retornar falso si todavia hay casillas disponibles para jugar" do
    expect(@board.isFinalize).to eq(false)
  end

  it "finalize deberia retornar verdadero si ya no hay casillas disponibles para jugar" do
    for i in 0..8
      @board.applyMove(i, 1)
    end
    expect(@board.isFinalize).to eq(true)
  end

  it "checkWinner deberia retonar cero cuando no hay ganadores en el tablero" do
    expect(@board.checkWinner).to eq(0)
  end

  it "checkWinner deberia retonar uno cuando el jugador1 gano con una barra horizonal creada desde la casilla 0" do
    @board.applyMove(0, 1)
    @board.applyMove(1, 1)
    @board.applyMove(2, 1)
    expect(@board.checkWinner).to eq(1)
  end

  it "checkWinner deberia retonar uno cuando el jugador1 gano con una barra horizonal creada desde la casilla 3" do
    @board.applyMove(3, 1)
    @board.applyMove(4, 1)
    @board.applyMove(5, 1)
    expect(@board.checkWinner).to eq(1)
  end

  it "checkWinner deberia retonar uno cuando el jugador1 gano con una barra horizonal creada desde la casilla 6" do
    @board.applyMove(6, 1)
    @board.applyMove(7, 1)
    @board.applyMove(8, 1)
    expect(@board.checkWinner).to eq(1)
  end

  it "checkWinner deberia retonar uno cuando el jugador1 gano con una barra vertical creada desde la casilla 0" do
    @board.applyMove(0, 1)
    @board.applyMove(3, 1)
    @board.applyMove(6, 1)
    expect(@board.checkWinner).to eq(1)
  end

  it "checkWinner deberia retonar uno cuando el jugador1 gano con una barra vertical creada desde la casilla 1" do
    @board.applyMove(1, 1)
    @board.applyMove(4, 1)
    @board.applyMove(7, 1)
    expect(@board.checkWinner).to eq(1)
  end

  it "checkWinner deberia retonar uno cuando el jugador1 gano con una barra vertical creada desde la casilla 2" do
    @board.applyMove(2, 1)
    @board.applyMove(5, 1)
    @board.applyMove(8, 1)
    expect(@board.checkWinner).to eq(1)
  end

  it "checkWinner deberia retonar uno cuando el jugador1 gano con una diagonal primaria" do
    @board.applyMove(0, 1)
    @board.applyMove(4, 1)
    @board.applyMove(8, 1)
    expect(@board.checkWinner).to eq(1)
  end

  it "checkWinner deberia retonar uno cuando el jugador1 gano con una diagonal secundaria" do
    @board.applyMove(2, 1)
    @board.applyMove(4, 1)
    @board.applyMove(6, 1)
    expect(@board.checkWinner).to eq(1)
  end

  it "checkWinner deberia retonar -1 cuando el jugador2 gano con una diagonal primaria" do
    @board.applyMove(0, -1)
    @board.applyMove(4, -1)
    @board.applyMove(8, -1)
    expect(@board.checkWinner).to eq(-1)
  end
end
